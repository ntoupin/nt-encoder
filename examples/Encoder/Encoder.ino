/*
   @file Encoder.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of encoder utilisation.
*/

#include <NT_Encoder.h>

Encoder_t Encoder_1;

void setup()
{
  Encoder_1.Configure(0.1);
  Encoder_1.Attach_Int(1, 2);
  Encoder_1.Map_Int(Encoder_1_Output_A, Encoder_1_Output_B);
  Encoder_1.Init();
}

void loop()
{
  Serial.print("Motor position : ");
  Serial.print(Encoder_1.Get_Position());
  Serial.print(" deg / ");

  Serial.print("Motor speed : ");
  Serial.print(Encoder_1.Get_Speed());
  Serial.print(" deg/s / ");

  Serial.print("Motor acceleration : ");
  Serial.print(Encoder_1.Get_Acceleration());
  Serial.print(" deg/s2.");
}

void Encoder_1_Output_A()
{
  Encoder_1.Output_A.State = (bool)digitalRead(Encoder_1.Output_A.Pin);
  Encoder_1.Compute();
}

void Encoder_1_Output_B()
{
  Encoder_1.Output_B.State = (bool)digitalRead(Encoder_1.Output_B.Pin);
}
