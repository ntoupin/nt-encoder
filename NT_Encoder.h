/*
 * @file NT_Encoder.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Encoder function declarations.
 */

#ifndef NT_Encoder_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Encoder_h

enum Control_Type_t
{
	ERROR = 0,
	CLOCKWISE = 1,
	COUNTER_CLOCKWISE = 2
};

class Interrupt_t
{
public:
	bool Enable = FALSE;
	int Pin;
	int Pin_Type = INPUT_PULLUP; // INPUT, OUTPUT or INPUT_PULLUP
	int Interrupt_Type = RISING; // LOW, CHANGE, RISING or FALLING
	bool State = FALSE;
	void (*Interrupt_Fct)();	
};

class Encoder_t
{
public:
	Encoder_t();
	bool Enable = FALSE;
	Interrupt_t Output_A;
	Interrupt_t Output_B;
	float Degree_Per_Count = 0;
	int Direction = ERROR;	
	void Configure(float Degree_Per_Count);
	void Attach_Int(int Pin_Output_A, int Pin_Output_B);
	void Init();
	void Deinit();
	void Map_Int(void *Interrupt_Output_A, void *Interrupt_Output_B);
	void Compute();
	float Get_Position();
	float Get_Speed();
	float Get_Acceleration();
	
private:
	long _Counter = 0;
	float _Position = 0; // Degree
	float _Speed = 0; // Degree per seconds
	float _Acceleration = 0; // Degree per seconds squared
	long _Timestamp = 0;
	long _Timestamp_Delta = 0;
};

#endif