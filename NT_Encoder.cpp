/*
 * @file NT_Encoder.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Encoder function definitions.
 */

#include "Arduino.h"
#include "NT_Encoder.h"

Encoder_t::Encoder_t()
{
}

void Encoder_t::Configure(float Degree_Per_Count)
{
	Degree_Per_Count = Degree_Per_Count;
}

void Encoder_t::Attach_Int(int Pin_Output_A, int Pin_Output_B)
{
	Output_A.Pin = Pin_Output_A;
	Output_B.Pin = Pin_Output_B;
}

void Encoder_t::Init()
{
	Enable = TRUE;
}

void Encoder_t::Deinit()
{
	Enable = FALSE;
}

void Encoder_t::Map_Int(void* Interrupt_Output_A, void* Interrupt_Output_B)
{
	Output_A.Interrupt_Fct = Interrupt_Output_A;
	Output_B.Interrupt_Fct = Interrupt_Output_B;
}

void Encoder_t::Compute()
{
	// Process direction encoder
	if ((Output_A.State == TRUE) && (Output_B.State == FALSE))
	{
		Direction = CLOCKWISE;
		_Counter += 1;
	}
	else if ((Output_A.State == TRUE) && (Output_B.State == TRUE))
	{
		Direction = COUNTER_CLOCKWISE;
		_Counter -= 1;
	}

	// Compute time delta and save new timestamp
	_Timestamp_Delta = micros() - _Timestamp;
	_Timestamp = micros();

	// Compute position, speed and acceleration
	_Position = _Counter * Degree_Per_Count;
	_Speed = (float)1000000 * Degree_Per_Count / _Timestamp_Delta;
	_Acceleration = _Speed / _Timestamp_Delta;
}

float Encoder_t::Get_Position()
{
	return _Position;
}

float Encoder_t::Get_Speed()
{
	return _Speed;
}

float Encoder_t::Get_Acceleration()
{
	return _Acceleration;
}